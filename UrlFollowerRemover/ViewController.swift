//
//  ViewController.swift
//  UrlFollowerRemover
//
//  Created by Marco Ferrati on 24/06/21.
//

import Cocoa
import SafariServices.SFSafariApplication
import SafariServices.SFSafariExtensionManager

let appName = "UrlFollowerRemover"
let extensionBundleIdentifier = "marcoferrati.UrlFollowerRemover.Extension"

class ViewController: NSViewController {

    @IBOutlet var appImage: NSImageView!
    @IBOutlet var appNameLabel: NSTextField!
    @IBOutlet var optionsStack: NSStackView!
    
    let defaults = UserDefaults(suiteName: "marcoferrati.UrlFollowerRemover.group")
    
    @IBOutlet var switch_openInNewTab: NSSwitch!
    var settings_openInNewTab: Bool = true
    var string_settings_openInNewTab: String = "openInNewTab"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appNameLabel.stringValue = appName
        self.appImage.image = #imageLiteral(resourceName: "icon-b-512")
        
        settings_openInNewTab = defaults!.bool(forKey: string_settings_openInNewTab)
        switch_openInNewTab.state = settings_openInNewTab ? .on : .off
        
        refreshOrInitializeExtensionStatus()
    }
    
    @IBAction func refreshExtensionStatus(_ sender: Any) {
        refreshOrInitializeExtensionStatus()
    }
    
    @IBAction func openSafariExtensionPreferences(_ sender: AnyObject?) {
        SFSafariApplication.showPreferencesForExtension(withIdentifier: extensionBundleIdentifier) { error in
            guard error == nil else {
                self.appNameLabel.stringValue = NSLocalizedString("cannot_open_safari_extensions_settings", comment: "Error for inability to open Safari preferences")
                return
            }

        }
    }
    
    @IBAction func change_openInNewTab(_ sender: NSSwitch) {
        let newState: Bool = sender.state == .on
        settings_openInNewTab = newState
        defaults!.set(newState, forKey: string_settings_openInNewTab)
    }
    
    func setSomenthingWentWrong(){
        self.appNameLabel.stringValue = NSLocalizedString("something_went_wrong_while_getting_the_extension_status", comment: "Error for inability to get extension status")
    }
    
    func setAppNameLabel(state: SFSafariExtensionState) {
        if (state.isEnabled) {
            self.optionsStack.isHidden = false;
            self.appNameLabel.stringValue = NSLocalizedString("extension_is_currently_on", comment: "Extension status ON")
        } else {
            self.optionsStack.isHidden = true;
            self.appNameLabel.stringValue = NSLocalizedString("extension_is_currently_off", comment: "Estension status OFF")
        }
    }
    
    func refreshOrInitializeExtensionStatus(){
        SFSafariExtensionManager.getStateOfSafariExtension(withIdentifier: extensionBundleIdentifier) { state, error in
            guard let state = state, error == nil else {
                self.setSomenthingWentWrong()
                return
            }
            
            DispatchQueue.main.async {
                self.setAppNameLabel(state: state)
            }
        }
    }

}
