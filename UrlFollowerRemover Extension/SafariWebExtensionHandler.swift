//
//  SafariWebExtensionHandler.swift
//  UrlFollowerRemover Extension
//
//  Created by Marco Ferrati on 24/06/21.
//

import SafariServices
import os.log

let SFExtensionMessageKey = "message"

class SafariWebExtensionHandler: NSObject, NSExtensionRequestHandling {

	func beginRequest(with context: NSExtensionContext) {
        let item = context.inputItems[0] as! NSExtensionItem
        let message = item.userInfo?[SFExtensionMessageKey]
        guard let messageOK = message as? [String: String] else {
            NSLog("Message is nilm or not convertible")
            return
        }

        let response = NSExtensionItem()
        
        switch messageOK["message"] {
        case "getSettings":
            NSLog("Request for settings")
            guard let defaults = UserDefaults(suiteName: "marcoferrati.UrlFollowerRemover.group") else {
                NSLog("ERROR: cannota access to SharedUserDefaults")
                return
            }
            NSLog("Setting openInNewTab=\(defaults.bool(forKey: "openInNewTab"))")
            let settings_newTab = defaults.bool(forKey: "openInNewTab") 
            response.userInfo = [ SFExtensionMessageKey: [ "openInNewTab": settings_newTab ] ]
        default:
            NSLog("Message not recognized")
            response.userInfo = [ SFExtensionMessageKey: [ "error": "no action for \(message ?? "No message")" ] ]
        }

        context.completeRequest(returningItems: [response]) { status in
            NSLog("Request completed with status: \(status)")
        }
    }
    
}
