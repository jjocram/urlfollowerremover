var settings = undefined

function getUrlFromClick(e){
    tag = e.target.parentNode;
    i = 0
    
    while(tag.tagName != 'A' && (i < 10)){
        //console.log("Link non individuato, we found: " + t.tagName);
        tag = tag.parentNode;
        i = i + 1;
    }
    
    if (i === 10){
        return null;
    } else {
        return tag.getAttribute("href");
    }
}

browser.runtime.sendMessage({message: "getSettings"}).then(value => {
    settings = value;
});

document.body.onclick = function(e){
    urlToGo = getUrlFromClick(e);
    console.log(settings);
    if (urlToGo === null) {
        //Could not find the url, keep the page default behaviour
        return true;
    }
    
    if (urlToGo.indexOf("facebook.com") !== -1) {
        //Opening a facebook page, stay in the same page
        return true
    }
    
    window.open(urlToGo, settings.openInNewTab ? "_blank" : "_self");
    return false;
    
};
